package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import java.awt.Color;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) 
			{ 
				readJTable();
			
			
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 678, 708);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ListarPassageiros");
		lblNewLabel.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 15));
		lblNewLabel.setBounds(10, 30, 304, 65);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 135, 726, 268);
		contentPane.add(scrollPane);
		
		JTPassageiros = new JTable();
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"ID", "NOME", "GENERO", "RG", "CPF", "Endere\u00E7o", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTPassageiros);
		
		JButton btnCadastrarPassageiro = new JButton("CADASTRAR");
		btnCadastrarPassageiro.setBackground(Color.CYAN);
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
            cp.setVisible(true);
			}
		});
		btnCadastrarPassageiro.setFont(new Font("Old English Text MT", Font.BOLD, 18));
		btnCadastrarPassageiro.setBounds(34, 461, 235, 100);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                // Verificar linha selecionada
                if (JTPassageiros.getSelectedRow() != -1) {
                    JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
                    ap.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
                }

                readJTable();
            }
		});
		btnAlterarPassageiro.setForeground(Color.RED);
		btnAlterarPassageiro.setFont(new Font("Century Schoolbook", Font.PLAIN, 17));
		btnAlterarPassageiro.setBounds(296, 461, 318, 108);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("EXCLUIR");
		btnExcluirPassageiro.setForeground(Color.MAGENTA);
		btnExcluirPassageiro.setFont(new Font("Goudy Stout", Font.PLAIN, 32));
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                if (JTPassageiros.getSelectedRow() != -1) {
                    int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
                    if (opcao == 0) {
                        PassageiroDAO dao = new PassageiroDAO();
                        Passageiro p = new Passageiro();
                        p.setIdPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
                        dao.delete(p);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
                }

                readJTable();
            }
		});
		btnExcluirPassageiro.setBounds(108, 596, 516, 65);
		contentPane.add(btnExcluirPassageiro);
	 readJTable();
	}
	public void readJTable() {
        DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
        modelo.setNumRows(0);
        PassageiroDAO pdao = new PassageiroDAO();
        for(Passageiro p : pdao.read()) {
            modelo.addRow(new Object[] {
                    p.getIdPassageiro(),
                    p.getNome(),
                    p.getGenero(),
                    p.getRG(),
                    p.getCPF(),
                    p.getEndereco(),
                    p.getEmail(),
                    p.getTelefone()
            });
        }
    }

	public Passageiro read(int id) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Passageiro p = new Passageiro();

        try {
            stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idPassageiro=? LIMIT 1;");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs != null && rs.next()) {
                p.setIdPassageiro(rs.getInt("idPassageiro"));
                p.setNome(rs.getString("nome"));
                p.setGenero(rs.getInt("genero"));
                p.setRG(rs.getString("rg"));
                p.setCPF(rs.getString("CPF"));
                p.setEmail(rs.getString("email"));
                p.setTelefone(rs.getString("telefone"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

        return p;
    }

public void update(Passageiro p) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE passageiro SET nome=?, genero=?, rg=?, cpf=?, endereco=?, email=?, telefone=? WHERE idPassageiro=?;");
            stmt.setString(1, p.getNome());
            stmt.setInt(2, p.getGenero());
            stmt.setString(3, p.getRG());
            stmt.setString(4, p.getCPF());
            stmt.setString(5, p.getEndereco());
            stmt.setString(6, p.getEmail());
            stmt.setString(7, p.getTelefone());
            stmt.setInt(8, p.getIdPassageiro());

            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }
	
}
