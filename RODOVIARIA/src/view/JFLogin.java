package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setBackground(new Color(240, 240, 240));
		setFont(new Font("Arial Black", Font.PLAIN, 12));
		setTitle("Sistema Rodoviaria");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 544, 543);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("BEM-VINDO AO SISTEMA DA RODOVIARIA");
		lblNewLabel.setFont(new Font("Bauhaus 93", Font.PLAIN, 24));
		lblNewLabel.setBounds(10, 10, 520, 58);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("DADOS ");
		lblNewLabel_1.setFont(new Font("Candara", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(0, 70, 173, 33);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("USUARIO");
		lblNewLabel_2.setBounds(2, 127, 116, 43);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Senha");
		lblNewLabel_3.setBounds(0, 189, 118, 50);
		contentPane.add(lblNewLabel_3);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(10, 160, 297, 33);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(10, 232, 320, 43);
		contentPane.add(txtSenha);
		
		JButton btnLogar = new JButton("Login");
		btnLogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnLogar.setBounds(160, 396, 312, 81);
		contentPane.add(btnLogar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(10, 396, 132, 84);
		contentPane.add(btnCancelar);
	}
}
