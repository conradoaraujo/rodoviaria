package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import java.awt.Window.Type;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JRadioButton;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCPF;
	private JTextField txtRG;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("Cadastro de Usuario");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 567, 622);
		contentPane = new JPanel();
		contentPane.setToolTipText("Cadastro de Usuario");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel farofa = new JLabel("Nome Completo");
		farofa.setBounds(10, 80, 241, 57);
		contentPane.add(farofa);
		
		JLabel ibcpf = new JLabel("CPF");
		ibcpf.setBounds(10, 147, 149, 38);
		contentPane.add(ibcpf);
		
		JLabel ibrg = new JLabel("RG");
		ibrg.setBounds(-1, 207, 170, 38);
		contentPane.add(ibrg);
		
		JLabel ibeendereco = new JLabel("Endere\u00E7o");
		ibeendereco.setBounds(10, 270, 200, 57);
		contentPane.add(ibeendereco);
		
		JLabel ibemail = new JLabel("Email");
		ibemail.setBounds(10, 337, 159, 57);
		contentPane.add(ibemail);
		
		JLabel ibltel = new JLabel("Telefone");
		ibltel.setBounds(-1, 407, 175, 49);
		contentPane.add(ibltel);
		
		JLabel lblNewLabel_6 = new JLabel("CADASTRO");
		lblNewLabel_6.setFont(new Font("Snap ITC", Font.PLAIN, 15));
		lblNewLabel_6.setBounds(211, 10, 159, 49);
		contentPane.add(lblNewLabel_6);
		
		txtNome = new JTextField();
		txtNome.setBounds(-1, 118, 308, 30);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(10, 177, 300, 38);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		txtRG = new JTextField();
		txtRG.setBounds(0, 244, 307, 30);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(-1, 308, 338, 43);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(-1, 375, 320, 38);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(10, 447, 320, 57);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		
		
		
		JButton btnNewButton_1 = new JButton("Cancelar");
		btnNewButton_1.setForeground(Color.CYAN);
		btnNewButton_1.setFont(new Font("MingLiU_HKSCS-ExtB", Font.BOLD | Font.ITALIC, 14));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				dispose();
			}
		});
		btnNewButton_1.setBounds(417, 518, 126, 57);
		contentPane.add(btnNewButton_1);
	
		
		JRadioButton rdbtnGeneroMasculino = new JRadioButton("Masculino");
		rdbtnGeneroMasculino.setBounds(349, 122, 149, 49);
		contentPane.add(rdbtnGeneroMasculino);
		
		JRadioButton rdbtnGeneroFeminino = new JRadioButton("Feminino");
		rdbtnGeneroFeminino.setBounds(351, 185, 126, 21);
		contentPane.add(rdbtnGeneroFeminino);
		
		JRadioButton rdbtnGeneroOutro = new JRadioButton("Outros");
		rdbtnGeneroOutro.setBounds(349, 228, 139, 30);
		contentPane.add(rdbtnGeneroOutro); 
		
		ButtonGroup genero = new ButtonGroup();
        genero.add(rdbtnGeneroMasculino);
        genero.add(rdbtnGeneroFeminino);
        genero.add(rdbtnGeneroOutro);
        
        JButton btnNewButton = new JButton(" ENVIAR CADASTRO");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                Passageiro p = new Passageiro();
                PassageiroDAO dao = new PassageiroDAO();

                p.setNome(txtNome.getText());
                p.setRG(txtRG.getText());
                p.setCPF(txtCPF.getText());
                p.setEndereco(txtEndereco.getText());
                p.setEmail(txtEmail.getText());
                p.setTelefone(txtTelefone.getText());
                if(rdbtnGeneroMasculino.isSelected()) {
                    p.setGenero(0);
                }else if(rdbtnGeneroFeminino.isSelected()) {
                    p.setGenero(1);
                }else if(rdbtnGeneroOutro.isSelected()){
                    p.setGenero(2);
                }

                dao.create(p);
                dispose();
            }


		});
		btnNewButton.setFont(new Font("Vladimir Script", Font.PLAIN, 29));
		btnNewButton.setBounds(0, 518, 419, 57);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("Limpar");
		btnNewButton_2.setFont(new Font("MS Reference Sans Serif", Font.PLAIN, 14));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
                txtRG.setText(null);
                txtCPF.setText(null);
                txtEndereco.setText(null);
                txtEmail.setText(null);
                txtTelefone.setText(null);
                genero.clearSelection();
                
			}
		});
		btnNewButton_2.setBounds(398, 465, 145, 38);
		contentPane.add(btnNewButton_2);
        
	}
	
	
}
